<?php
/**
 * Snippets are a great way to store code snippets for reuse or to keep your templates clean.
 * This header snippet is reused in all templates.
 * It fetches information from the `site.txt` content file and contains the site navigation.
 * More about snippets: https://getkirby.com/docs/guide/templates/snippets
 */
?>

<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Share:wght@400;700&display=swap" rel="stylesheet">

  <!-- The title tag we show the title of our site and the title of the current page -->
  <title><?= $site->title() ?> | <?= $page->title() ?></title>

  <!-- Stylesheets can be included using the `css()` helper. Kirby also provides the `js()` helper to include script file. -->
    <?= css(['assets/css/index.css', '@auto']) ?>
    <?= js('assets/js/jquery.js') ?>
</head>
<body>

  <div class="page">
    <div class="wrapper">
      <div class="row">

        <nav id="menu" class="menu">
          <a href="<?= $site->url() ?>">Home</a>
          <a class="anchor" href="<?= $site->url() ?>/#contact">Contact</a>
          <!-- <a class="anchor" href="<?= $site->url() ?>/#news">News &amp; Projects</a> -->
        </nav>
      </div>
    </div>
