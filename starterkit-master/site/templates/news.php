<?php
/**
 * Templates render the content of your pages.
 * They contain the markup together with some control structures like loops or if-statements.
 * This template is responsible for rendering all the subpages of the `notes` page.
 * The `$page` variable always refers to the currently active page.
 * To fetch the content from each field we call the field name as a method on the `$page` object, e.g. `$page->title()`.
 * Snippets like the header and footer contain markup used in multiple templates. They also help to keep templates clean.
 * More about templates: https://getkirby.com/docs/guide/templates/basics
 */
?>

<?php snippet('header') ?>

<div class="wrapper news-wrapper">
  <div class="row small-row blog-row">
    <header class="note-header intro">
      <h1 class="article-title"><?= $page->title() ?></h1>
      <div class="share">
        <a class="share-twitter" href="https://twitter.com/home?status=https://highsofleeds.co.uk/ ">share on twitter</a>
        <a class="share-fb" href="">share on facebook</a>
      </div>
      <time class="article-date"><?= $page->date()->toDate('d F Y') ?></time>
    </header>
  </div>

  <div class="row small-row blog-row">
    <div class="main-article">
      <?= $page->text()->kt() ?>
    </div>
  </div>
</div>

<?php snippet('footer') ?>
