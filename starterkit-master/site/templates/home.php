<?php
/**
 * Templates render the content of your pages.
 * They contain the markup together with some control structures like loops or if-statements.
 * The `$page` variable always refers to the currently active page.
 * To fetch the content from each field we call the field name as a method on the `$page` object, e.g. `$page->title()`.
 * This home template renders content from others pages, the children of the `photography` page to display a nice gallery grid.
 * Snippets like the header and footer contain markup used in multiple templates. They also help to keep templates clean.
 * More about templates: https://getkirby.com/docs/guide/templates/basics
 */
?>

<?php snippet('header') ?>

<div class="wrapper primary-themeSection">
	<div class="row" id="homeAbout-row">
		<div class="col twoCol">
			<div id="about-box">
				<div class="aboutBox-inner">
					<h2 class="section-title">About Me</h2>
					<div class="box left nonBg" >
						<p>My name is <strong>Soraya Taraszka</strong> and I am a <strong>web developer</strong>. I work professionally in development since 2016 and now I want to expand my skills into <strong>freelance</strong> work! </p>
					</div>
				</div>
			</div>
		</div>

		<div class="col twoCol">
			<div class="box primary-bg right" id="covidInfo-box">
				<h2 class="section-title">FREELANCE OPPORTUNITIES</h2>
					<p>I am specialised in Wordpress websites and front-end development, but I can also do a website in Kirby or a small Jquery applications.</p><p>  <strong>For details enquire below!</strong></p>
			</div>
		</div>
	</div>
</div>

<div class="wrapper secondary-themeSection secondary-themeSection" id="contact">

	<div class="row">
		<div class="col ">
			<div class="box left primary-bg" id="contact-box">
				<h2 class="section-title">Contact me at...</h2>
				<p class="social-link"><img src="<?= $kirby->url('assets') ?>/img/mail.svg" alt="mail icon"><a href="mailto:sorayataraszka@gmail.com" target="_blank">sorayataraszka@gmail.com</a></p>
				<p class="social-link"><img src="<?= $kirby->url('assets') ?>/img/twitter-shadow.svg" alt="twitter icon"><a href="https://twitter.com/ImSorayaT" target="_blank">@sorayat</a></p>
			</div>
		</div>

		<!-- <div class="col twoCol">
			<h2 class="section-title">FREELANCE OPPORTUNITIES</h2>
			<div class="box nonBg left">
				<p>I am specialising in Wordpress websites and front-end development, but I can also do a small Jquery applications. Tell me about the project and I’ll give you an honest answer whether I can do it or not within two working days!</p>
			</div>
		</div> -->
	</div>
</div>

<!-- <div class="wrapper primary-themeSection" id="news">
	<div class="row title-row">
		<h2 class="section-title">NEWS &amp; PROJECTS</h2>
	</div>
	<div class="row">
		<?php
		if ($portfolioAndNews = pages('news')){ ?>
		<ul class="grid">
			<?php
			foreach ($portfolioAndNews->children()->listed()->sortBy('date', 'desc') as $lala): ?>
				<li class="<?= $lala->parent() ?>">
					<div class="placeholderCard">
					</div>
					<h3 class="card-title"><a href="<?= $lala->url() ?>"><?= $lala->title() ?></a></h3>
					<div class="card-content">
						<?= $lala->tidbit() ?>
					</div>
					<a class="button" href="<?= $lala->url() ?>"> Read More </a>
				</li>
			<?php
			endforeach ?>
		</ul>
		<?php } ?>
	</div>
</div> -->


<?php snippet('footer') ?>
